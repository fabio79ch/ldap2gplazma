#!/usr/bin/python

# Puppet Managed File

# 12/12/2011 by martinelli

"""
THE SCRIPT CREATES 2 dCache gPlazma FILEs, USUALLY TO BE STORED INTO: 
/etc/grid-security/grid-vorolemap
/etc/grid-security/storage-authzdb        
ACCORDING TO THE INFO FOUND INSIDE AN OPENLDAP SERVER 

FOR REFERENCE: 
http://webcms.ba.infn.it/cms-software/index.html/index.php/Main/CmsUserMapping
https://wiki.chipp.ch/twiki/bin/view/CmsTier3/NodeTypeStorageElement#Automatic_creation_of_the_files
"""

# grid-vorolemap_template example
"""
# This file maps VOMS information to local user accounts

# DN   VOMS    local_account

$DNs_VOMSROLE_LINUXUSERs


#
"*" "/ops" opsuser
"*" "/ops/Role=lcgadmin" opsuser
#
"*" "/dteam" dteamuser
#
"*" "/cms" cmsuser
"*" "/cms/Role=production" cmsuser

"""


# storage-authzdb_template example
"""
version 2.1
authorize cmsuser   read-only  501 500 / / /
authorize opsuser   read-write 800 800 / / /
authorize dteamuser read-write 810 810 / / /

$AUTHORIZE_USERs_PERMs_UID_GID

"""


import ldap
import sys
import os
import stat
import getpass
import random,string
from optparse import OptionParser
import socket
from string import Template

######## MAIN VARIABLEs ###################
global myldaptree
##########################################

################## MAIN ###################
usage = "usage: %prog options "
parser = OptionParser(usage)
parser.add_option("-v", "--verbose", action="store_true", dest="verbose", help="Produce verbose information during the execution", default="")
parser.add_option("-H", "--host-ssl", dest="ldaphost", help="An SSL LDAP server like 't3ldap01.psi.ch' ( domain psi.ch matters for SSL )", default="")
parser.add_option("-D", "--output-directory", dest="output_dir", help="The directory where to store the grid-vorolemap and storage-authzdb files produced, default is the current directory.", default=".")

(options, args) = parser.parse_args()

NO_OPTIONS_PROVIDED={'ldaphost': '', 'verbose': ''}

if options  == NO_OPTIONS_PROVIDED:
      parser.print_help()
      sys.exit(0)

if options.ldaphost == "":
      print "ERROR: -Hs or --host-ssl is a compulsory option"
      sys.exit(1)
else: 
      try: 
         socket.gethostbyname(options.ldaphost)
      except socket.gaierror, e:
         print "The hostname %s is not mapped inside the DNS, maybe a typo?" % options.ldaphost
         sys.exit(1)


# The output directory provided like an option must to be a real directory 
assert stat.S_ISDIR( os.stat(options.output_dir)[stat.ST_MODE] )

# without a reachable SSL LDAP server we can't do anything
try:
      myldaptree = ldap.initialize("ldaps://"+options.ldaphost)
except ldap.LDAPError, e:
      print e.message['info']
      if type(e.message) == dict and e.message.has_key('desc'):
         print e.message['desc']
      else:         print e
      sys.exit(1)


# Preliminary coherence checks 
assert os.path.isfile('grid-vorolemap_template')
assert os.path.isfile('storage-authzdb_template')
assert os.path.getsize('grid-vorolemap_template')  > 0
assert os.path.getsize('storage-authzdb_template') > 0
grid_vorolemap_template_readlines  = ''.join(open('grid-vorolemap_template').readlines()  )
storage_authzdb_template_readlines = ''.join(open('storage-authzdb_template').readlines() )
# Placeholders must to be present inside the 2 template files
assert grid_vorolemap_template_readlines.find("$DNs_VOMSROLE_LINUXUSERs")        != -1
assert storage_authzdb_template_readlines.find("$AUTHORIZE_USERs_PERMs_UID_GID") != -1
####################

# Preparing the template objects
myStringTemplate01 = Template(grid_vorolemap_template_readlines)
myStringTemplate02 = Template(storage_authzdb_template_readlines)

USERsLIST  = myldaptree.search_s( base="ou=People,dc=cmst3,dc=psi,dc=ch", scope=ldap.SCOPE_SUBTREE, filterstr="(subjectDN~=/)", attrlist=['uid','subjectDN','uidNumber','gidNumber','loginShell'],attrsonly=0 )

myAUTHORIZE_USERs_PERMs_UID_GID=""
myDNs_VOMSROLE_LINUXUSERs=""

# we use one 'for' for both files
for user in USERsLIST:
    if user[1]['loginShell'] == '/sbin/nologin' : pass # we skip ghost users 
    assert ''.join(user[1]['subjectDN']) != ''
    myString01 = '"'
    myString01 += ''.join(user[1]['subjectDN'])
    myString01 += '" '

    while len(myString01)< 120 : myString01+=' '

    myString01 += '"/cms" '
    myString01 += ''.join(user[1]['uid'])
    myDNs_VOMSROLE_LINUXUSERs+=myString01 + '\n'

    myString02 = 'authorize '
    myString02 += ''.join(user[1]['uid']) + ' '

    while len(myString02)< 30 : myString02+=' '

    myString02 += ' read-write   '
    myString02 += ''.join(user[1]['uidNumber'])+' '

    while len(myString02)< 50 : myString02+=' '

    if user[1]['gidNumber'] == ['500'] :  # 500 is the CMS group
            myString02 += ''.join(              '    500' ) + ' ' 
            #myString02 += ' / / /'
    else : 
            myString02 += ''.join(user[1]['gidNumber']) + ',500 '
            #myString02 += ' / / /'
    myString02 += ' / / /'

    myAUTHORIZE_USERs_PERMs_UID_GID+=myString02 + '\n'

#print myDNs_VOMSROLE_LINUXUSERs
#print myAUTHORIZE_USERs_PERMs_UID_GID

myFile01 = open( options.output_dir + '/grid-vorolemap', 'w')
myFile02 = open( options.output_dir + '/storage-authzdb','w')

myFile01.write( myStringTemplate01.substitute(DNs_VOMSROLE_LINUXUSERs=myDNs_VOMSROLE_LINUXUSERs) )
if options.verbose != "" : print 'Created file: ' + options.output_dir + '/grid-vorolemap' 
myFile02.write( myStringTemplate02.substitute(AUTHORIZE_USERs_PERMs_UID_GID=myAUTHORIZE_USERs_PERMs_UID_GID))
if options.verbose != "" : print 'Created file: ' + options.output_dir + '/storage-authzdb'
##############################################

sys.exit(0)
 
